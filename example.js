//Create a new roller, this is a collection of tables that can be rolled at once
//You can create multiple rollers with different tables to only roll some tables at once
var roller = charRoller();

//Create a table
//charRoller.Table(name, DiceRoll)
var levelTable1 = charRoller.Table("Level", charRoller.DiceRoll(1, 20));

//charRoller.DiceRoll(numberOfDice, diceType, add)
//  Creates a DiceRoll object
//  For example:
//    charRoller.DiceRoll(2, 20, 5) creates a roll equivelent to 2d20+5
//    charRoller.DiceRoll("2d20+5") is the same as above

//You can also use the DiceRoll shorthand instead of the DiceRoll object
//meaning these two statements are equivelent:
//charRoller.Table("Level", charRoller.DiceRoll(1, 20));
//charRoller.Table("Level", "d20");

//table.addOutput(start, end, value)
//  Adds an output range to table from start(inclusive) to end(inclusive) and with the return value of value.
//  Meaning if when rolling on the table the rolled value is within the start and end of output range the
//  value associated with the range is returned.
//
//  The end parameter is optional, if it isn't provided it will be the same as start resulting in a range of 1
//  If the value parameter is a Table or a DiceRoll it will be rolled if returned,
//  meaning a table can have a random result on some output ranges, or a seperate table on some output ranges.
levelTable1.addOutput(1, 11, 1);
levelTable1.addOutput(12, 14, 2);
levelTable1.addOutput(15, 16, 3);
levelTable1.addOutput(17, 18, 4);
levelTable1.addOutput(19, 5);
levelTable1.addOutput(20, 6);

//Add the table to the roller
roller.addTable(levelTable1);

//You can also use the DiceRoll shorthand instead of a DiceRoll object
var levelTable2 = charRoller.Table("Level", "d100");
levelTable2.addOutput(1, 50, 1);
levelTable2.addOutput(51, 70, 2);
levelTable2.addOutput(71, 82, 3);
levelTable2.addOutput(83, 88, 4);
levelTable2.addOutput(89, 93, 5);
levelTable2.addOutput(94, 97, 6);
levelTable2.addOutput(98, 99, 7);
levelTable2.addOutput(100, 8);
roller.addTable(levelTable2);

var levelTableLow = charRoller.Table("Level(Low Level)", "d20");
levelTableLow.addOutput(1, 10, 1);
levelTableLow.addOutput(11, 16, 2);
levelTableLow.addOutput(17, 20, 3);
roller.addTable(levelTableLow);

var levelTableMid = charRoller.Table("Level(Mid Level)", "d20");
levelTableMid.addOutput(1, 10, 4);
levelTableMid.addOutput(11, 16, 5);
levelTableMid.addOutput(17, 20, 6);
roller.addTable(levelTableMid);

var levelTableHigh = charRoller.Table("Level(High Level)", "d20");
levelTableHigh.addOutput(1, 10, 7);
levelTableHigh.addOutput(11, 16, 8);
levelTableHigh.addOutput(17, 20, 9);
roller.addTable(levelTableHigh);

var ageTable = charRoller.Table("Age", "d20");
ageTable.addOutput(1, 2, "d6+13");
ageTable.addOutput(3, 4, "d10+17");
ageTable.addOutput(5, 8, "d10+24");
ageTable.addOutput(9, 11, "d10+30");
ageTable.addOutput(12, 15, "d20+40");
ageTable.addOutput(16, 17, "d10+60");
ageTable.addOutput(18, 19, "d10+70");
ageTable.addOutput(20, "d20+80");
roller.addTable(ageTable);

//A table that doesn't have any output ranges will return the value of the roll instead of doing a
//lookup in the output ranges, this means that if a 1 is rolled on the table, 1 is returned
var humanMaleHeightTable = charRoller.Table("Humans Male Height", "2d10+59"),
	humanMaleWeightTable = charRoller.Table("Humans Male Weight", "6d10+140"),
	humanFemaleHeightTable = charRoller.Table("Humans Female Height", "2d10+55"),
	humanFemaleWeightTable = charRoller.Table("Humans Female Weight", "6d10+90"),
	elvesMaleHeightTable = charRoller.Table("Elves Male Height", "d10+57"),
	elvesMaleWeightTable = charRoller.Table("Elves Male Weight", "3d10+90"),
	elvesFemaleHeightTable = charRoller.Table("Elves Female Height", "d10+52"),
	elvesFemaleWeightTable = charRoller.Table("Elves Female Weight", "3d10+70"),
	dwarvesMaleHeightTable = charRoller.Table("Dwarves Male Height", "2d6+35"),
	dwarvesMaleWeightTable = charRoller.Table("Dwarves Male Weight", "5d4+70"),
	dwarvesFemaleHeightTable = charRoller.Table("Dwarves Female Height", "2d6+33"),
	dwarvesFemaleWeightTable = charRoller.Table("Dwarves Female Weight", "5d4+65"),
	halflingsMaleHeightTable = charRoller.Table("Halflings Male Height", "2d4+31"),
	halflingsMaleWeightTable = charRoller.Table("Halflings Male Weight", "2d6+45"),
	halflingsFemaleHeightTable = charRoller.Table("Halflings Female Height", "2d4+30"),
	halflingsFemaleWeightTable = charRoller.Table("Halflings Female Weight", "2d6+40"),
	gnomesMaleHeightTable = charRoller.Table("Gnomes Male Height", "d6+27"),
	gnomesMaleWeightTable = charRoller.Table("Gnomes Male Weight", "3d4+25"),
	gnomesFemaleHeightTable = charRoller.Table("Gnomes Female Height", "d6+26"),
	gnomesFemaleWeightTable = charRoller.Table("Gnomes Female Weight", "3d4+23");
roller.addTable(humanMaleHeightTable);
roller.addTable(humanMaleWeightTable);
roller.addTable(humanFemaleHeightTable);
roller.addTable(humanFemaleWeightTable);
roller.addTable(elvesMaleHeightTable);
roller.addTable(elvesMaleWeightTable);
roller.addTable(elvesFemaleHeightTable);
roller.addTable(elvesFemaleWeightTable);
roller.addTable(dwarvesMaleHeightTable);
roller.addTable(dwarvesMaleWeightTable);
roller.addTable(dwarvesFemaleHeightTable);
roller.addTable(dwarvesFemaleWeightTable);
roller.addTable(halflingsMaleHeightTable);
roller.addTable(halflingsMaleWeightTable);
roller.addTable(halflingsFemaleHeightTable);
roller.addTable(halflingsFemaleWeightTable);
roller.addTable(gnomesMaleHeightTable);
roller.addTable(gnomesMaleWeightTable);
roller.addTable(gnomesFemaleHeightTable);
roller.addTable(gnomesFemaleWeightTable);

var godsTable = charRoller.Table("Gods", "d100");

var commonGods = charRoller.Table("Common Gods", "d12");
commonGods.addOutput(1, 3, "Astair");
commonGods.addOutput(4, "Felumbra");
commonGods.addOutput(5, "Malkis");
commonGods.addOutput(6, 8, "Martha");
commonGods.addOutput(9, 10, "Nadinis");
commonGods.addOutput(11, "Tempos");
commonGods.addOutput(12, "Voraci");
godsTable.addOutput(1, 70, commonGods);

var uncommonGods = charRoller.Table("Uncommon Gods", "d12");
uncommonGods.addOutput(1, "Agepa");
uncommonGods.addOutput(2, "Bellum");
uncommonGods.addOutput(3, "Chis");
uncommonGods.addOutput(4, 5, "Dorbaff /Guam");
uncommonGods.addOutput(6, "Efra");
uncommonGods.addOutput(7, "Jexel");
uncommonGods.addOutput(8, "John");
uncommonGods.addOutput(9, "Nerual");
uncommonGods.addOutput(10, "Quantarius");
uncommonGods.addOutput(11, "Reluna");
uncommonGods.addOutput(12, "Sayor");
godsTable.addOutput(71, 90, uncommonGods);

var rareGods = charRoller.Table("Rare Gods", "d12");
rareGods.addOutput(1, 2, "Illumis");
rareGods.addOutput(3, "Kee");
rareGods.addOutput(4, "Matrigal");
rareGods.addOutput(5, "Ponos");
rareGods.addOutput(6, "Relkor");
rareGods.addOutput(7, "Solt");
rareGods.addOutput(8, "Terrasa");
rareGods.addOutput(9, 10, "Velmontarious");
rareGods.addOutput(10, "Quantarius");
rareGods.addOutput(12, "Woamatoor");
godsTable.addOutput(91, 100, rareGods);

roller.addTable(godsTable);

//Roll the roller, this rolls on all the table, it returns an array of results,
//this array can be joined to make a single string to make it easier to read.
console.log(roller.roll().join());

//You can also roll on a single table,
//here we roll on the age table, this returns a result, so to turn it into a more
//readable format we the toString() function
console.log(ageTable.roll().toString());