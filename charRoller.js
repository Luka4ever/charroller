charRoller = (function charRollerInit(){
	/**
	 * Creates a new charRoller instance.
	 *
	 * @class
	 */
	var charRoller = function charRoller() {
		if (!(this instanceof charRoller)) {
			return new charRoller();
		}
		this.tables = [];
	};
	/**
	 * DiceRoll string pattern.
	 *
	 * @constant
	 * @type {RegExp}
	 */
	charRoller.diceRollPattern = /^(\d)?d(\d+)(?:\+(\d+))?$/;
	/**
	 * Adds a Table to the collection.
	 *
	 * @param {Table} table - The Table to add to the collection.
	 */
	charRoller.prototype.addTable = function addTable(table) {
		this.tables[this.tables.length] = table;
	};
	/**
	 * Rolls on all Tables in the collection.
	 *
	 * @returns {Array.<charRoller.Result>}
	 */
	charRoller.prototype.roll = function roll() {
		var result = [],
			i;
		for (i = 0; i < this.tables.length; i++) {
			result[i] = this.tables[i].roll();
		}
		return result;
	};
	/**
	 * Creates a new Table instance.
	 *
	 * @class
	 * @param {string} name                         - The name of the table.
	 * @param {charRoller.DiceRoll|string} diceRoll - The DiceRoll to use on the table.
	 */
	charRoller.Table = function Table(name, diceRoll) {
		if (!(this instanceof charRoller.Table)) {
			return new charRoller.Table(name, diceRoll);
		}
		this.name = name;
		this.diceRoll = charRoller.DiceRoll.fromString(diceRoll);
		this.output = [];
	};
	/**
	 * Adds an output range to the table.
	 *
	 * @param {number} start                                                           - The start of the output range.
	 * @param {number} [end=start]                                                     - The end of the output range (inclusive).
	 * @param {(number|string|charRoller.Table|charRoller.DiceRoll|charRoller)} result - The reslut of the output range.
	 */
	charRoller.Table.prototype.addOutput = function addOutput(start, end, result) {
		if (arguments.length == 2) {
			result = end;
			end = start;
		}
		this.output[this.output.length] = {
			start: start,
			end: end,
			result: charRoller.DiceRoll.fromString(result)
		};
	};
	/**
	 * Gets the ouput of the table for a certain value.
	 *
	 * @param {number} value - The value to lookup on the table.
	 * @returns {(charRoller.Result|number|string)}
	 */
	charRoller.Table.prototype.getOutput = function getOutput(value) {
		var i,
			item,
			result;
		for (i = 0; i < this.output.length; i++) {
			item = this.output[i];
			if (item.start <= value && value <= item.end) {
				return item.result;
			}
		}
		return undefined;
	};
	/**
	 * Rolls on the table.
	 *
	 * @returns {charRoller.Result}
	 */
	charRoller.Table.prototype.roll = function roll() {
		var roll = this.diceRoll.roll(),
			result = (this.output.length > 0 ? this.getOutput(roll.valueOf()) : roll.valueOf());
		return new charRoller.Result(this, result, roll);
	};
	/**
	 * Creates a string representing the Table.
	 *
	 * @returns {string}
	 */
	charRoller.Table.prototype.toString = function toString() {
		return this.name + " (" + this.diceRoll + ")";
	};
	/**
	 * Creates a new Result instance.
	 *
	 * @class
	 * @param {(charRoller.Table|charRoller.DiceRoll)} source - The source of the result.
	 * @param {(number|string)} result                        - The result value.
	 * @param {charRoller.DiceRollResult} roll                - The value rolled on the DiceRoll.
	 */
	charRoller.Result = function Result(source, result, roll) {
		if (!(this instanceof charRoller.Result)) {
			return new charRoller.Result(source, result, roll);
		}
		if (charRoller.Table.prototype.isPrototypeOf(result) ||
			charRoller.DiceRoll.prototype.isPrototypeOf(result)) {
			result = result.roll();
		}
		this.source = source;
		this.value = result;
		this.roll = roll;
	}
	/**
	 * Creates a string representing the Result.
	 *
	 * @returns {string}
	 */
	charRoller.Result.prototype.toString = function toString() {
		return this.source + ": (" + this.roll.toString() + ") " + this.value;
	};
	/**
	 * Creates a new DiceRoll instance.
	 *
	 * @class
	 * @param {(number|string)} numberOfDice - The number of dice to roll, can also be DiceRoll shorthand.
	 * @param {number} [diceType]            - The dice type to roll, if numberOfDice is not DiceRoll
	 *                                         shorthand this is a required parameter.
	 * @param {number} [add]                 - The number to add.
	 */
	charRoller.DiceRoll = function DiceRoll(numberOfDice, diceType, add) {
		if (typeof numberOfDice === "string") {
			return charRoller.DiceRoll.fromString(numberOfDice);
		}
		if (!(this instanceof charRoller.DiceRoll)) {
			return new charRoller.DiceRoll(numberOfDice, diceType, add);
		}
		this.numberOfDice = numberOfDice;
		this.diceType = diceType;
		this.add = add | 0;
	};
	/**
	 * Attempts to create a new DiceRoll instance from a string.
	 *
	 * @class
	 * @param {string} str - The string to try to convert to a DiceRoll from DiceRoll shorthand.
	 * @returns {(charRoller.DiceRoll|string)} A new DiceRoll instance based on str or the first parameter.
	 */
	charRoller.DiceRoll.fromString = function fromString(str) {
		var result;
		if (typeof str === "string" && charRoller.diceRollPattern.test(str)) {
			result = charRoller.diceRollPattern.exec(str);
			return new charRoller.DiceRoll(
					(result[1] == undefined ? 1 : Number.parseInt(result[1], 10)),
					Number.parseInt(result[2], 10),
					(result[3] == undefined ? undefined : Number.parseInt(result[3], 10))
				);
		}
		return str;
	};
	/**
	 * Rolls the dice.
	 *
	 * @returns {charRoller.DiceRollResult}
	 */
	charRoller.DiceRoll.prototype.roll = function roll() {
		var result = new charRoller.DiceRollResult(this.add),
			i;
		for (i = 0; i < this.numberOfDice; i++) {
			result.addRoll(Math.floor(Math.random() * this.diceType) + 1);
		}
		return result;
	};
	/**
	 * Creates a string representing the DiceRoll.
	 *
	 * @returns {string}
	 */
	charRoller.DiceRoll.prototype.toString = function toString() {
		return (this.numberOfDice === 1 ? "" : this.numberOfDice) + "d" + this.diceType + (this.add === 0 ? "" : "+" + this.add);
	};
	/**
	 * Attempts to create a new DiceRoll instance from a string.
	 *
	 * @class
	 * @param {number} add - The value to add to the roll.
	 */
	charRoller.DiceRollResult = function DiceRollResult(add) {
		if (!(this instanceof charRoller.DiceRollResult)) {
			return new charRoller.DiceRollResult();
		}
		this.rolls = [];
		this.add = add;
	};
	/**
	 * Adds a roll to the DiceRollResult.
	 *
	 * @param {number} value - The value to add.
	 */
	charRoller.DiceRollResult.prototype.addRoll = function addRoll(value) {
		this.rolls[this.rolls.length] = value;
	};
	/**
	 * Gets the sum of the DiceRollResult.
	 *
	 * @returns {number} The sum of the DiceRollResult.
	 */
	charRoller.DiceRollResult.prototype.valueOf = function valueOf() {
		var i,
			sum = this.add;
		for (i = 0; i < this.rolls.length; i++) {
			sum = sum + this.rolls[i];
		}
		return sum;
	};
	/**
	 * Creates a string representing the DiceRollResult.
	 *
	 * @returns {string}
	 */
	charRoller.DiceRollResult.prototype.toString = function toString() {
		return "[" + this.rolls.join(", ") + "]" + (this.add == 0 ? "" : "+" + this.add) + "=" + this.valueOf();
	};
	return charRoller;
})();